import React from 'react';
import {Provider} from 'react-redux';
import {ReduxRouter} from 'redux-react-router';

import configureStore from './store/configureStore';

const store = configureStore();

export default (
  <Provider store={store}>
    {() =>
      <ReduxRouter />
    }
  </Provider>
);
