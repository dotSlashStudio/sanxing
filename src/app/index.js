import config from 'config';
import 'helpers/moment-zhtw';

const path = location.hash && location.hash.replace('#', '');

// document.querySelector('#favicon').href = require('./favicon.ico');

require.ensure([], (require) => {
  const React = require('react');
  const app = require('./app');

  require('./styles/bootstrap.css');
  require('./styles/common.css');
  require('./styles/react-slick.css');

  React.render(app, document.getElementById('app'));
});

// fetchMeAsync()
// .catch(() => {
//   location.replace(`/login.html?path=${path}`);
// })
// .then((res) => {
//   localStorage.me = JSON.stringify(res);
//   ga('create', config.gaId, {
//     userId: res.email,
//   });

//   require.ensure([], (require) => {
//     const React = require('react');
//     const app = require('./app');

//     require('./styles/bootstrap.css');
//     require('./styles/common.css');

//     React.render(app, document.getElementById('app'));
//   });
// })
// .catch((err) => {
//   console.error(err);
//   ga('send', 'exception', {
//     exDescription: err.message,
//     exFatal: true,
//   });
// });
