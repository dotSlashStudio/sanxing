import {LOADING, LOADED} from 'constants/actionTypes';

export function loading(id) {
  return {
    type: LOADING,
    payload: id,
  };
}

export function loaded(id) {
  return {
    type: LOADED,
    payload: id,
  };
}
