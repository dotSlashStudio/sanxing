import {pushState} from 'redux-react-router';

export function updateQueries(payload = {}) {
  return (dispatch, getState) => {
    const {query} = getState().router;
    return dispatch(pushState(null, '/search', {
      ...query,
      ...payload,
    }));
  };
}

export function resetQueries(payload = {}) {
  return pushState(null, '/search', payload);
}
