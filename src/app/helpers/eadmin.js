import request from './superagent';
import config from 'config';

const {baseUrl} = config.eadmin;
const {access_token} = localStorage;

export function fetchMeAsync() {
  return (
    request.get(baseUrl + '/account/me')
    .query({
      access_token,
    })
    .endAsync()
    .get('body')
  );
}
