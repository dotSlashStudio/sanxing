import {LOADING, LOADED} from 'constants/actionTypes';

export default function loadingReducer(state = {}, action) {
  if (action.type === LOADING) {
    return {
      ...state,
      [action.payload]: true,
    };
  }
  if (action.type === LOADED) {
    return {
      ...state,
      [action.payload]: false,
    };
  }
  return state;
}
