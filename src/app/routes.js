import React from 'react';
import Route from 'react-router/lib/Route';
import Redirect from 'react-router/lib/Redirect';

import Base from 'components/base/Base';
import Home from 'components/home/Home';
import Profile from 'components/profile/Profile';
import Production from 'components/production/Production';
import Culture from 'components/culture/Culture';
import Contact from 'components/contact/Contact';
import Technology from 'components/technology/Technology';

import Enterprise from 'components/culture/enterprise';
import Brand from 'components/culture/brand';
import Human from 'components/culture/human';

import Series1 from 'components/production/series1';
import Series2 from 'components/production/series2';
import Series3 from 'components/production/series3';
import Series4 from 'components/production/series4';
import Series5 from 'components/production/series5';
import Series6 from 'components/production/series6';
import Series7 from 'components/production/series7';

import Tech1 from 'components/technology/tech1';
import Tech2 from 'components/technology/tech2';
import Tech3 from 'components/technology/tech3';
import Tech4 from 'components/technology/tech4';
import Tech5 from 'components/technology/tech5';

import Tech51 from 'components/technology/tech51';
import Tech52 from 'components/technology/tech52';
import Tech53 from 'components/technology/tech53';
import Tech54 from 'components/technology/tech54';
import Tech55 from 'components/technology/tech55';

export default (
  <Route component={Base}>
    <Route path="/" component={Home} />
    <Route path="/home" component={Home} />
    <Route path="/culture" component={Culture}>
      <Route path="enterprise" component={Enterprise} />
      <Route path="brand" component={Brand} />
      <Route path="human" component={Human} />
    </Route>
    <Route path="/profile" component={Profile} />
    <Route path="/production" component={Production}>
      <Route path="series1" component={Series1} />
      <Route path="series2" component={Series2} />
      <Route path="series3" component={Series3} />
      <Route path="series4" component={Series4} />
      <Route path="series5" component={Series5} />
      <Route path="series6" component={Series6} />
      <Route path="series7" component={Series7} />
    </Route>
    <Route path="/technology" component={Technology}>
      <Route path="tech1" component={Tech1} />
      <Route path="tech2" component={Tech2} />
      <Route path="tech3" component={Tech3} />
      <Route path="tech5" component={Tech5} />
      <Route path="tech4" component={Tech4}>
        <Route path="tech51" component={Tech51} />
        <Route path="tech52" component={Tech52} />
        <Route path="tech53" component={Tech53} />
        <Route path="tech54" component={Tech54} />
        <Route path="tech55" component={Tech55} />
      </Route>
    </Route>
    <Route path="/contact" component={Contact} />
  </Route>
);
