import React, {Component, PropTypes} from 'react';

import './ProfileStyle.css';

export default class Profile extends Component {

  render() {
    return (
      <div>
        <div className="banner">
          <img src={require('./images/banner.jpg')} alt=""/>
        </div>

        <div className="container">
          <div className="margin-top-xl profile-title">
            <span className="title-cn">三興概况</span>
            <span className="title-en">Combinde Profile</span>
          </div>

          <div className="row">
            <div className="col-md-6">
              <div className="profile-pic">
                <img src={require('./images/profile.png')} alt=""/>
                <div className="row">
                  <div className="col-md-6">
                    <img src={require('./images/s1.png')} alt=""/>
                  </div>
                  <div className="col-md-6">
                    <img src={require('./images/s2.png')} alt=""/>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-6">
              <div className="profile-text">
                <p>广东省惠州市惠东县三興化工有限公司,是一家专注于鞋用粘胶剂的品牌公司。公司座落于广东省惠东县民营工业园,占地面积 三万多平方米。创办以来,三興人以优良的品质保障和诚恳务实的服务态度赢得了广大合作客户的认同和赞誉。</p>
                <p>经过15年的不懈努力和发展,三興化工有限公司拥有了先进的科研基地,卓越的技术人才以及完善的全套检测系统。为进一步满 足客户的需求,公司不断完善管理,提升科技,并与各知名学府和科研机构合作研发,尤其致力于环保型粘剂的研发。已经研发出了 免洗功能的系列产品并已经通过中国环保产品认证和I S0900 l质量管理体系的认证。</p>
                <p>三興化工有限公司本着 “诚信 创新 技术 服务” 的经营宗旨,愿以高质量的产品,专业化的技术服务,与所有的厂商、代理商与 经销商互利合作,共同进步,双惠双赢。</p>
              </div>
            </div>
          </div>
          <div className="margin-top-xl profile-title">
            <span className="title-cn">合作伙伴</span>
            <span className="title-en">Partners</span>
          </div>
          <div className="row">
            <div className="col-md-2">
              <img src={require('./images/p1.jpg')} alt=""/>
            </div>
            <div className="col-md-2">
              <img src={require('./images/p2.jpg')} alt=""/>
            </div>
            <div className="col-md-2">
              <img src={require('./images/p3.jpg')} alt=""/>
            </div>
            <div className="col-md-2">
              <img src={require('./images/p4.jpg')} alt=""/>
            </div>
            <div className="col-md-2">
              <img src={require('./images/p5.jpg')} alt=""/>
            </div>
            <div className="col-md-2">
              <img src={require('./images/p6.jpg')} alt=""/>
            </div>
          </div>
          <div className="row">
            <div className="col-md-2">
              <img src={require('./images/p7.jpg')} alt=""/>
            </div>
            <div className="col-md-2">
              <img src={require('./images/p8.jpg')} alt=""/>
            </div>
            <div className="col-md-2">
              <img src={require('./images/p9.jpg')} alt=""/>
            </div>
            <div className="col-md-2">
              <img src={require('./images/p10.jpg')} alt=""/>
            </div>
            <div className="col-md-2">
              <img src={require('./images/p11.jpg')} alt=""/>
            </div>
            <div className="col-md-2">
              <img src={require('./images/p12.jpg')} alt=""/>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

