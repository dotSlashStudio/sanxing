import React, {Component, Proptypes} from 'react';

export default class Tech5 extends Component {

  render() {
    return (
      <div>
        <div className="title2">
          <i className="fa fa-tags margin-right-md"></i>
          <span>制鞋工业常用胶造剂</span>
        </div>
       <ul className="items">
          <li>1、PU胶。又称PU糊、聚氨酯胶、优丽胶、白胶。主要用于贴底及成型；</li>
          <li>2、接枝胶。叉称药水糊、药水胶、氯丁接枝胶；主要用于攀邦、包跟、EVA大底复合；</li>
          <li>3、黄胶。叉称黄糊、氯丁胶、鞋用万能胶。主要用于攀邦、包中底、贴合包鞋跟及手袋折边等用；</li>
          <li>4、接枝黄胶。即接枝胶与黄胶的混和体。现手套专用胶一一黄胶的D型(400D)即是此类;</li>
          <li>5、中底胶。SBS万能胶。主要用于中底复合、箱包手袋等；</li>
          <li>6、天然橡胶乳液。包括生胶、粉胶、亚么尼亚胶等；</li>
          <li>7、各类鞋材表面处理剂；</li>
          <li>8、配套产品：如清洁剂、港宝水、天那水、固化剂、硫化剂等。</li>
        </ul>
      </div>
    )
  }
}
