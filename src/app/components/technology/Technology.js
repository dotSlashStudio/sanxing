import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';

export default class Technology extends Component {

  render() {
    return (
      <div>
        <div className="banner margin-bottom-lg">
          <img src={require('./images/banner.jpg')} alt=""/>
        </div>
        <div className="container">
          <div className="margin-top-lg row">
            <div className="sidebar col-xs-3">
              <Link to="/technology/tech1" activeClassName="active">
                <i className="fa fa-gears margin-right-md"></i>
                <span>环保喷胶特性对比</span>
              </Link>
              <Link to="/technology/tech2" activeClassName="active">
                <i className="fa fa-gears margin-right-md"></i>
                <span>鞋面喷胶</span>
              </Link>
              <Link to="/technology/tech3" activeClassName="active">
                <i className="fa fa-gears margin-right-md"></i>
                <span>鞋底/鞋面材料辨别方法</span>
              </Link>
              <Link to="/technology/tech4/tech51" activeClassName="active">
                <i className="fa fa-gears margin-right-md"></i>
                <span>粘接技术</span>
              </Link>
              <Link to="/technology/tech5" activeClassName="active">
                <i className="fa fa-gears margin-right-md"></i>
                <span>常见问题及解决方法</span>
              </Link>
            </div>
            <div className="main-content col-xs-9">
              {this.props.children}
            </div>
          </div>
        </div>
      </div>
    )
  }
}
