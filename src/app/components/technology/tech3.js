import React, {Component, Proptypes} from 'react';

export default class Tech3 extends Component {

  render() {
    return (
      <div>
        <div className="profile-title">
          <span className="title-cn">鞋底/鞋面材料辨别方法</span>
        </div>
        <div>
          <table className="table table-bordered table-striped table-hover">
            <tr>
              <th> 材料类型<br />
              <br /></th>
              <th> 辨别方法<br />
              <br /></th>
            </tr>
            <tr>
              <td><strong> PVC底<br />
              <br />
              </strong></td>
              <td> 有注射口，剪一小片，用火点燃，火焰跳动带绿边，离火自熄。<br /></td>
            </tr>
            <tr>
              <td><strong> TPR底<br />
              <br />
              </strong></td>
              <td> 有注射口，剪一小片，用火点燃，火焰稳定无绿边，离火不会自熄。<br /></td>
            </tr>
            <tr>
              <td><strong> 橡胶底<br />
              <br />
              </strong></td>
              <td> 无注射口，剪一小片，点燃，有废轮胎烧焦昧。<br /></td>
            </tr>
            <tr>
              <td><strong> EVA底<br />
              <br />
              </strong></td>
              <td> 无注射口，剪一小片，点燃，有烧蜡烛焦味及融化滴落现象。<br /></td>
            </tr>
            <tr>
              <td><strong> PVC革<br />
              <br />
              </strong></td>
              <td> 剪一小片，点燃，火焰跳动带绿边。</td>
            </tr>
            <tr>
              <td><strong> PU革<br />
              <br />
              </strong></td>
              <td> 剪一小片，点燃，火焰稳定无绿边。</td>
            </tr>
            <tr>
              <td><strong> 牛皮<br />
              <br />
              </strong></td>
              <td> 质地硬挺，用砂轮打磨易光滑发亮。</td>
            </tr>
            <tr>
              <td><strong> 猪、羊皮<br />
              <br />
              </strong></td>
              <td> 质地柔软，用砂轮打磨易起毛。<br />
                <br />
              </td>
            </tr>
            <tr>
              <td><strong> 尼龙布<br />
                <br />
              </strong></td>
              <td> 表面光滑，剪一小片，点燃,有熔融滴落现象。<br /></td>
            </tr>
            <tr>
              <td><strong> 帆布<br />
                <br />
                <br />
              <br />
              </strong></td>
              <td> 剪一小片，点燃，无熔融滴落现象。<br /></td>
            </tr>
          </table>
        </div>
      </div>
    )
  }
}
