import React, {Component, Proptypes} from 'react';

export default class Tech1 extends Component {

  render() {
    return (
      <div>
        <div className="profile-title">
          <span className="title-cn">环保喷胶特性对比</span>
        </div>
        <div>
          <table className="table table-bordered table-striped table-hover">
            <tr>
              <td> 胶水种类<br />
              <br /></td>
              <td> 生胶糊<br />
                （11KG/桶）<br />
              <br /></td>
              <td> 热熔胶<br />
                （1.1KG/块<br />
              <br /></td>
              <td> 三興108无苯环保<br />
                喷胶（15KG/桶）<br />
                <br />
              </td>
              <td> 备注<br />
              <br /></td>
            </tr>
            <tr>
              <td> 一般使用<br />
                部位或材质<br />
              <br /></td>
              <td> 适甩于泡沫皮革等材<br />
                料的接著<br />
                <br />
             </td>
              <td> 前港与后港或邦里与针<br />
                车所用材质(防水材料<br />
              除外)<br /></td>
              <td> 鞋面，内里，港宝，贴台，<br />
                EVA棉，海绵，皮革，油皮均<br />
                可，代替黄胶，粉胶，热熔胶<br />
              几乎针对所有材质都可以使用</td>
              <td> 供应商提供<br />
              <br /></td>
            </tr>
            <tr>
              <td> 物性<br />
              <br /></td>
              <td> 浅黄色粘液，酯类气<br />
                球，沸点82—120℃，<br />
                应避免之状况：强光<br />
                ，应避免之物质：过<br />
                气化物、强酸、强碱<br />
              、强气化剂</td>
              <td> 米白色固体，加热后有<br />
                剩鼻味，软化点82±3℃<br />
                ，应避免之状况：远离火<br />
                源，应避免之物蘑：强碱<br />
              或强酸</td>
              <td> 透明液体，芳香昧，沸点：<br />
                110.6℃，应避免之状况：远<br />
                离火源，应避免之物质：强碱<br />
              或强酸<br /></td>
              <td> 供应商提供物质安<br />
                全资料表<br />
                <br />
              </td>
            </tr>
            <tr>
              <td> 化学成份<br />
              <br /></td>
              <td> 1、120带溶剂油70-82%<br />
                2、天然橡胶10%-15%<br />
                3、松香5-10%．<br />
                4、台成橡胶3-5%<br />
              <br /></td>
              <td> 1、热塑树脂树脂20一60％<br />
                2、增粘树脂20一60％<br />
              3、石腊10一20％<br /></td>
              <td> 1、环保溶剂60-65％<br />
                2、合成橡胶20—25％<br />
                3、树脂20一25％<br />
              <br /></td>
              <td> 供应商提供物质安<br />
                全资料表<br />
                <br />
              </td>
            </tr>
            <tr>
              <td> PPE个人<br />
                防护事项<br />
              <br /></td>
              <td> 1、有效抽风或配戴防<br />
                毒口罩<br />
                2、手直接接触时配戴<br />
                防毒手套<br />
                3、避免皮趺直接接触<br />
              该化学品<br /></td>
              <td> 1、呼吸防护具<br />
                2、橡胶手套<br />
                3、穿着标准工作服<br />
              <br /></td>
              <td> 1、带防护口罩<br />
                2、带手套<br />
                3、带防护面具<br />
              4、穿防护衣<br /></td>
              <td> 供应商提供物质安<br />
                全资料表<br />
                <br />
              </td>
            </tr>
            <tr>
              <td> 无郊损耗<br />
              <br /></td>
              <td> 暂未提供<br />
              <br /></td>
              <td> 暂未提供<br />
              <br /></td>
              <td> 暂未提供<br />
              <br /></td>
              <td> IE提供<br />
              <br /></td>
            </tr>
            <tr>
              <td> 学习成本<br />
              <br /></td>
              <td> 刷胶3至7天学会，但熟<br />
                练要一个月，若要包滚<br />
                口或贴帮里等其它工序<br />
              则需要三个月熟练<br /></td>
              <td> 一天会操作，<br />
                半个月后熟练<br />
                <br />
              </td>
              <td> 一天会操作，半个月后熟练<br />
                <br />
              </td>
              <td> 一天会操作，半个月后熟练<br />
                <br />
              </td>
            </tr>
            <tr>
              <td> 每双用量<br />
              <br /></td>
              <td> 57.30g/双<br />
              <br /></td>
              <td> 16.29g/双<br />
              <br /></td>
              <td> 4.82g/双<br />
              <br /></td>
              <td> 数据IE提供<br />
              <br /></td>
            </tr>
            <tr>
              <td> 用时成本<br />
              <br /></td>
              <td> 517秒/双<br />
              <br /></td>
              <td> 215秒/双<br />
              <br /></td>
              <td> 221秒/双<br />
              <br /></td>
              <td> 数据lE提供，当时技<br />
                术员要求喷单面，虽<br />
                然用时少，但用量显<br />
                示也比较多，后再多<br />
                次测试其他型体时都<br />
              需耍喷双面</td>
            </tr>
          </table>
        </div>
      </div>
    )
  }
}
