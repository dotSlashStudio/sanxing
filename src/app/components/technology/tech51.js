import React, {Component, Proptypes} from 'react';

export default class Tech5 extends Component {

  render() {
    return (
      <div>
        <div className="title2">
          <i className="fa fa-tags margin-right-md"></i>
          <span>防范影响粘接</span>
        </div>
        <ul className="items">
          <li> 1、防止灰尘——灰尘是影响粘合力的关键之一；如果被粘物表面粘上灰尘，且没有清除干净，涂胶之后胶粘剂的渗透力被灰尘阻隔，就会影
          响粘合力，已涂刷好胶粘剂的半成品，如表面粘上灰尘，则互相粘合后粘合力更差。因此应在操作现场，安装防尘设施或保持环境干净无尘。</li>
          <li>2、防止水份——水份是影响粘合力的另一关键，被粘物水份含量过高，水份占据被粘物的表面，胶粘剂无法渗透被粘物，就会影响粘合力。
          另一种原因，被粘物虽然是干燥的，但涂好胶之后，遇到雨天，湿度大，在胶粘剂的表层凝聚了水份，如果烘烤温度和时间不够，粘合力也会
          达不到要求。因此应保持环境干燥通风良好。以及使用存放太久的鞋材时，为避免水份影晴应先送行加热烘干，再进行粘接处理。</li>
          <li>3、防止油类——油类是影响粘合力的关键之三，被粘物表面粘有油类之后，胶粘剂无法渗入被粘物空隙，只能浮在表面，粘台力会大大下
          降，严重的会出现脱胶，解决办法是先把油渍去除。</li>
        </ul>
        <img src={require('./images/shous2.png')} alt=""/>
      </div>
    )
  }
}
