import React, {Component, Proptypes} from 'react';

export default class Tech5 extends Component {

  render() {
    return (
      <div>
        <div className="title2">
          <i className="fa fa-tags margin-right-md"></i>
          <span>胶水剂选择</span>
        </div>
        <p>选择胶粘剂正确与否，为影响粘接效果的首要原因，以下因素可做为选择胶粘剂的依据</p>
        <div className="title3">
          <span>1、材质种类</span>
        </div>
        <ul className="items">
          <li>天然材质如纸板等用黄胶、中底胶即可得到良好的粘结效果，一般塑料、橡胶都会选择用接枝胶、PU胶进行粘接。</li>
        </ul>
        <div className="title3">
          <span>2、材质构造</span>
        </div>
        <ul className="items">
          <li>多孔性材料(如真皮、油皮、帆布、绒布等)较易吸收胶粘剂，应选用粘度较高者，避免胶粘剂过度渗入材质内导致表面欠胶；</li>
          <li>非多孔性材质(如PVC、TPR等)，表面平整光滑，选择用低粘度的胶粘剂，较易涂布均匀，能得到均匀的干燥效果。</li>
        </ul>
        <div className="title3">
          <span>3、作流程及习惯</span>
        </div>
        <ul className="items">
          <li>上胶次数量----对多孔性材料，若仅上胶一次，须选择高粘高固含的胶粘剂，若上胶两次，用低固含的胶粘剂即可；</li>
          <li>烘箱长度、输送带速度----长度较长，速度较慢者，可选择干燥速度较慢，粘性维持时间较长的胶粘剂，反之亦然；</li>
          <li>工作速度----若速度固定，且保持稳定，可选择粘性维持时间较短的胶粘剂，以提高初期粘接力。</li>
        </ul>
        <p>粘接强度的要求及耐热、耐水、耐油性。各胶粘剂的性质均有差异，虽可加入硬化剂进行改善，但每一种胶粘剂加入硬化剂后所发挥的效果亦不
同，所以应根据需求选择相应的胶粘剂。</p>
        <img src={require('./images/shous3.png')} alt=""/>
      </div>
    )
  }
}
