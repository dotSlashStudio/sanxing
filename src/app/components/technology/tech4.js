import React, {Component, Proptypes} from 'react';
import {Link} from 'react-router';

export default class Tech4 extends Component {

  render() {
    return (
      <div>
        <div className="profile-title clearfix">
          <div className="pull-left">
            <span className="title-cn">粘接技术</span>
          </div>
          <div className="pull-right">
            <ol className="breadcrumbs">
              <li><Link to="/technology/tech4/tech51">防范影响粘接</Link></li>
              <li><Link to="/technology/tech4/tech52">粘接欠佳分析</Link></li>
              <li><Link to="/technology/tech4/tech53">制鞋工业常用胶造剂</Link></li>
              <li><Link to="/technology/tech4/tech54">胶水剂选择</Link></li>
              <li><Link to="/technology/tech4/tech55">粘接操作工艺</Link></li>
            </ol>
          </div>
        </div>

        <div>
          {this.props.children}
        </div>
      </div>
    );
  }
}
