import React, {Component, Proptypes} from 'react';

export default class Tech2 extends Component {

  render() {
    return (
      <div>
        <div className="profile-title">
          <span className="title-cn">鞋面喷胶</span>
        </div>
        <div>
          <div className="title2">
            <i className="fa fa-tags margin-right-md"></i>
            <span>品名： 108</span>
          </div>
          <div className="title2">
            <i className="fa fa-tags margin-right-md"></i>
            <span>特性说明：</span>
          </div>
          <ul>
            <li>1、无苯环保，无色无味，操作方便、安全，干燥快，节能降耗且节省人力，省电。</li>
            <li>2、粘着时间长，粘性好，无需加热。</li>
            <li>3、耐黄变、耐老化性好。(注：黄变及老化测试可达到4.5级)</li>
            <li>4、节约成本，可代替粉胶、生胶、黄胶及热熔胶，大幅度降低胶水用量。</li>
          </ul>
          <div className="row">
            <div className="col-xs-6">
              <div className="title2">
                <i className="fa fa-tags margin-right-md"></i>
                <span>性状：</span>
              </div>
              <ul>
                <li>1、外观：108喷胶为无色透明液体</li>
                <li>2、粘度：200±50CPS</li>
                <li>3、固成分40%一50%</li>
              </ul>
              <div className="title2">
                <i className="fa fa-tags margin-right-md"></i>
                <span>成分：</span>
              </div>
              <p>合成橡胶、环保溶剂、增粘树脂。</p>
            </div>
            <div className="col-xs-6">
              <img src={require('./images/shous.png')} alt=""/>
            </div>
          </div>

          <div className="title2">
            <i className="fa fa-tags margin-right-md"></i>
            <span>产品贮藏及运输：</span>
          </div>
          <p>108环保喷胶为液体状物体，其贮藏宜在5℃一40℃放存，常温密闭贮藏时间正常为6个月。其运输须注意防止高温高压，须
防止剧烈碰撞。</p>
<div className="title2">
            <i className="fa fa-tags margin-right-md"></i>
            <span>主要用途：</span>
          </div>
          <p>主要用于鞋面、手袋，内里贴合，折边，以及鞋面港宝与内里贴合等。</p>
        </div>
      </div>
    );
  }
}
