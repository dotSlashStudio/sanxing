import React, {Component, Proptypes} from 'react';

export default class Series5 extends Component {

  render() {
    return (
      <div>
        <div className="profile-title">
          <span className="title-cn">清洁剂/橡胶糊系列</span>
        </div>
        <div>
          <table className="table table-bordered table-striped table-hover">
            <tr>
              <th> 产品名称
              </th>
              <th> 外观
              </th>
              <th> 主成分
              </th>
              <th> 粘度
              </th>
              <th> 主要用途及特性
              </th>
            </tr>
            <tr>
              <td>801</td>
              <td> 无色透明
              </td>
              <td> 溶剂
              </td>
              <td>
              </td>
              <td>去污力特强，适用于橡胶、IP前处理。
                    </td>
            </tr>
            <tr>
              <td>802</td>
              <td> 无色透明</td>
              <td> 溶剂
              </td>
              <td></td>
              <td>去污力一般，可用于清洗鞋面污渍。      </td>
            </tr>
            <tr>
              <td>803</td>
              <td>浅黄色粘液
                </td>
              <td> 溶剂
                </td>
              <td> 30000±
                2000CP5
                </td>
              <td>去污力一强，适用于清洗鞋面残胶。
                </td>
            </tr>
            <tr>
              <td>801A</td>
              <td> 浅黄色粘液
                </td>
              <td> 合成树脂
                </td>
              <td> 200000±
                1000CP5
                </td>
              <td>粘性维持时间长，接着力佳。适用于各种泡棉、
          布、皮革、纸品等接着。</td>
            </tr>
            <tr>
              <td>801B</td>
              <td>浅黄色粘液
              </td>
              <td> 合成树脂
                </td>
              <td> 24000±
                100CP5
                </td>
              <td> 粘性维持时间长，接着力佳。适用于各种泡棉、
                布、皮革、纸品等接着。
              </td>
            </tr>
            <tr>
              <td>803A</td>
              <td>浅黄色粘液
                </td>
              <td> 合成树脂
                </td>
              <td> 20000±
                1000CP5
                </td>
              <td> 粘性维持时间长，接着力佳。适用于各种泡棉、
                布、皮革、纸品等接着。
                </td>
            </tr>
            <tr>
              <td>803B</td>
              <td>浅黄色粘液
              </td>
              <td> 合成树脂
                </td>
              <td> 15000±
                1000CP5
                </td>
              <td> 粘性维持时间长，接着力佳。适用于各种泡棉、
                布、皮革、纸品等接着。
              </td>
            </tr>
            <tr>
              <td> 亚么尼亚胶
              </td>
              <td> 乳白色粘液
                </td>
              <td> 天然橡胶
                </td>
              <td> 90±
                20CP5
                </td>
              <td> 干燥快，易使用；夹布加工、纸品贴合、攀鞋。
                </td>
            </tr>
          </table>
        </div>
      </div>
    )
  }
}
