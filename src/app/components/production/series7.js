import React, {Component, Proptypes} from 'react';

export default class Series7 extends Component {

  render() {
    return (
      <div>
        <div className="profile-title">
          <span className="title-cn">其他类 </span>
        </div>
        <div>
          <table className="table table-bordered table-striped table-hover">
            <tr>
              <th> 产品名称
              </th>
              <th> 外观
              </th>
              <th> 主成分
              </th>
              <th> 名称
              </th>
              <th> 主要用途及特性
              </th>
            </tr>
            <tr>
              <td> 800H
              </td>
              <td> 透明粘液
              </td>
              <td> 合成树脂
              </td>
              <td> 硬化糊
              </td>
              <td> 硬度高、粘力佳、不变形，适用于各种需要硬化之
              套头，后跟内里之硬化成型。</td>
            </tr>
            <tr>
              <td>858B</td>
              <td> 黑色粘液
              </td>
              <td> 有机于无机颜料
              </td>
              <td> 色膏
              </td>
              <td> 配合溶剂型接着剂调整颜色用，在药水糊中有良好
              的分散性。</td>
            </tr>
            <tr>
              <td>PFE</td>
              <td> 黄色透明液
              </td>
              <td> 异氰酸盐
              </td>
              <td> 硬化剂
              </td>
              <td> 配合胶粘剂使用，提高粘合力，对橡胶效果特别
              明显。</td>
            </tr>
            <tr>
              <td>RN</td>
              <td> 无色透明液
              </td>
              <td> 异氰酸盐
              </td>
              <td> 硬化剂
              </td>
              <td> 耐黄变性好，配合PU胶使用，提高粘合力。
              </td>
            </tr>
            <tr>
              <td>RC</td>
              <td> 微黄透明液
              </td>
              <td> 异氰酸盐
              </td>
              <td> 硬化剂
              </td>
              <td> 配合PU胶使用，提高粘合力。</td>
            </tr>
          </table>
        </div>
      </div>
    )
  }
}
