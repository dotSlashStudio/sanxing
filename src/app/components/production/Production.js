import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';

export default class Production extends Component {

  render() {
    return (
      <div>
        <div className="banner margin-bottom-lg">
          <img src={require('./images/banner.jpg')} alt=""/>
        </div>
        <div className="container">
          <div className="margin-top-lg row">
            <div className="sidebar col-xs-3">
              <Link to="/production/series1" activeClassName="active">
                <i className="fa fa-cube margin-right-md"></i>
                <span>免洗底系列</span>
              </Link>
              <Link to="/production/series2" activeClassName="active">
                <i className="fa fa-cube margin-right-md"></i>
                <span>接枝胶系列</span>
              </Link>
              <Link to="/production/series3" activeClassName="active">
                <i className="fa fa-cube margin-right-md"></i>
                <span>贴合胶系列</span>
              </Link>
              <Link to="/production/series4" activeClassName="active">
                <i className="fa fa-cube margin-right-md"></i>
                <span>万能胶系列</span>
              </Link>
              <Link to="/production/series5" activeClassName="active">
                <i className="fa fa-cube margin-right-md"></i>
                <span>清洁剂/橡胶糊系列</span>
              </Link>
              <Link to="/production/series6" activeClassName="active">
                <i className="fa fa-cube margin-right-md"></i>
                <span>处理剂系列</span>
              </Link>
              <Link to="/production/series7" activeClassName="active">
                <i className="fa fa-cube margin-right-md"></i>
                <span>其他类</span>
              </Link>
            </div>
            <div className="main-content col-xs-9">
              {this.props.children}
            </div>
          </div>
        </div>
      </div>
    )
  }
}
