import React, {Component, Proptypes} from 'react';

export default class Series3 extends Component {

  render() {
    return (
      <div>
        <div className="profile-title">
          <span className="title-cn">贴合胶系列</span>
        </div>
        <div>
          <table className="table table-bordered table-striped table-hover">
            <tr>
              <th width='100'> 产品名称
              </th>
              <th> 外观
              </th>
              <th> 主成分
              </th>
              <th> 粘度
              </th>
              <th> 主要用途及特性
              </th>
            </tr>
            <tr>
              <td>808H</td>
              <td> 半透明液体</td>
              <td> 合成树脂
              </td>
              <td> 1100±
                100CP5
              </td>
              <td> 强力型，适用于浅色EVA/尼龙、EVA/布的贴合。
                    </td>
            </tr>
            <tr>
              <td>858</td>
              <td> 半透明液体
                    </td>
              <td> 合成树脂
              </td>
              <td> 6500±
                500CP5
              </td>
              <td> 强力型，适用于浅色高硬度EVA/尼龙及PVC/EVA，
              PVC/尼龙，PVC/PVC的贴合。</td>
            </tr>
            <tr>
              <td>868</td>
              <td> 半透明液体
              </td>
              <td> 合成树脂
              </td>
              <td> 6500±
                500CP5
              </td>
              <td> 通用型适用于高硬度EVA/尼龙及PVC/EVA，PVC/
              尼龙，PVC/PVC的贴合。      </td>
            </tr>
          </table>
        </div>
      </div>
    )
  }
}
