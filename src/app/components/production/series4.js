import React, {Component, Proptypes} from 'react';

export default class Series4 extends Component {

  render() {
    return (
      <div>
        <div className="profile-title">
          <span className="title-cn">万能胶系列</span>
        </div>
        <div>
          <table className="table table-bordered table-striped table-hover">
            <tr>
              <th width='100'> 产品名称
              </th>
              <th width='100'> 外观
              </th>
              <th width='100'> 主成分
              </th>
              <th> 粘度
              </th>
              <th> 主要用途及特性
              </th>
            </tr>
            <tr>
              <td>809</td>
              <td> 黄色粘液
              </td>
              <td> 合成树脂


              </td>
              <td> 2500±
                500CP5
              </td>
              <td> 粘性时间适中，用于中底、纸板巾合，攀帮、
                EVA、木材等粘合。
                    </td>
            </tr>
            <tr>
              <td>801</td>
              <td> 黄色粘液
                    </td>
              <td> 合成树脂
              </td>
              <td> 4000±
                500CP5
              </td>
              <td> 粘性时间适中，用于中底、纸板巾合，攀帮、
              EVA、木材等粘合。      </td>
            </tr>
            <tr>
              <td>801L</td>
              <td> 黄色粘液
                </td>
              <td> 合成树脂
                </td>
              <td> 3500±
                500CP5
                </td>
              <td> 快干，粘性维持时间适中。用于家具装修、纸
                板粘合，攀帮等用途。
                </td>
            </tr>
            <tr>
              <td>808</td>
              <td> 浅黄色粘液
                </td>
              <td> 合成树脂
                </td>
              <td> 10000±
                1000CP5
                </td>
              <td> 粘性维持时间长、耐黄变，用于鞋面针车，亦
                可用于浅色材料。
                </td>
            </tr>
            <tr>
              <td>825</td>
              <td> 黄色粘液
              </td>
              <td> 合成树脂
                </td>
              <td> 8000±
                500CP5
                </td>
              <td> 接着力强，耐热性佳。攀帮专用胶，对于硬纸
                板、木材、麻布等材质有较强粘性。

              </td>
            </tr>
            <tr>
              <td>805</td>
              <td> 黄色粘液 </td>
              <td> 合成树脂 </td>
              <td> 7000±
                500CP5 </td>
              <td> 适用于鞋内中底包跟、鞋垫、攀鞋等以及一般<br />
              材质的接着。<br /></td>
            </tr>
            <tr>
              <td>808</td>
              <td> 无色透明粘液<br />
              <br /></td>
              <td> 合成树脂 </td>
              <td> 2500±
                50CP5 </td>
              <td> 粘性维持时间长，接着力佳。适用各种泡棉<br />
                、布、皮革、纸品、木材等制品大面积之喷<br />
              涂接着。</td>
            </tr>
            <tr>
              <td>808A</td>
              <td>浅黄色粘液</td>
              <td> 合成树脂 </td>
              <td> 350±
                50CP5 </td>
              <td> 适用于PVC、普利龙、皮革、织布、金属、<br />
              木材、纸等之接着。<br /></td>
            </tr>
            <tr>
              <td>808B</td>
              <td>浅黄色粘液</td>
              <td> 合成树脂
                </td>
              <td> 250±
                50CP5
                </td>
              <td> 一般家具、建筑材料、皮革、布、水泥、麻<br />
                布、竹草、木砖、石材、普利龙、纸品等之<br />
              接着及软木成型用。</td>
            </tr>
          </table>
        </div>
      </div>
    )
  }
}
