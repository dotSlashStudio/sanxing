import React, {Component, Proptypes} from 'react';

export default class Series2 extends Component {

  render() {
    return (
      <div>
        <div className="profile-title">
          <span className="title-cn">接枝胶系列</span>
        </div>
        <div>
          <table className="table table-bordered table-striped table-hover">
            <tr>
              <th width='100'> 产品名称
              </th>
              <th> 外观
              </th>
              <th> 主成分
              </th>
              <th> 粘度
              </th>
              <th> 主要用途及特性
              </th>
            </tr>
            <tr>
              <td>885</td>
              <td> 淡黄色透明粘液
              </td>
              <td> CR/MMA
                接枝聚合物
              </td>
              <td> 2000±
                200CP5
              </td>
              <td> 适用于PVC、PU、橡胶、EVA、皮革等材质之接
              着。可用于女鞋、运动鞋底部贴合等各种粘合。</td>
            </tr>
            <tr>
              <td>885F</td>
              <td> 淡黄色半透明
                粘液
              </td>
              <td> CR/MMA
                接枝聚合物
              </td>
              <td> 2700±
                200CP5
              </td>
              <td> 耐热性好，适用于攀鞋，以及PVC、PU、EVA、
                橡胶等材料粘合。适合于三文治组合底，包跟以
              及箱包使用。</td>
            </tr>
            <tr>
              <td>885H</td>
              <td> 淡黄色透明粘液
              </td>
              <td> CR/MMA
                接枝聚合物
              </td>
              <td> 2700±
                200CP5
              </td>
              <td> 粘性维持时间长，操作性能佳。海滩鞋、包跟、
              室温干燥皮鞋用胶。</td>
            </tr>
            <tr>
              <td>810</td>
              <td> 淡黄色半透明
                液体
              </td>
              <td> CR/MMA
                接枝聚合物
              </td>
              <td> 2500±
                200CP5
              </td>
              <td> 适用于加硫鞋或成型使用。

              </td>
            </tr>
            <tr>
              <td>810S</td>
              <td> 淡黄色透明
                液体
              </td>
              <td> CR/MMA
                接枝聚合物
              </td>
              <td> 2200±
                200CP5
              </td>
              <td> 适用于加硫鞋或成型使用。

              </td>
            </tr>
            <tr>
              <td>813</td>
              <td> 淡黄色半透明
                液体
              </td>
              <td> CR/MMA
                接枝聚合物
              </td>
              <td> 2500±
                200CP5
              </td>
              <td> 耐黄变型，耐候性佳，初期粘力强，对PVC、橡
                胶、TPR、EVA有很强的粘合力，尤适合做白色
              的运动鞋。</td>
            </tr>
            <tr>
              <td>820</td>
              <td> 淡黄色透明液体
              </td>
              <td> CR/MMA
                接枝聚合物
              </td>
              <td> 2200±
                200CP5
              </td>
              <td> 粘性维持时间长，易操作。对PVC、橡胶、TPR
              、EVA有很强的粘合力，使用于手工操作。</td>
            </tr>
            <tr>
              <td>825</td>
              <td> 淡黄色透明液体
              </td>
              <td> CR/MMA
                接枝聚合物
              </td>
              <td> 2200±
                200CP5
              </td>
              <td> 性价比高，且操作方便容易。适用于PVC、橡胶
              、TPR、EVA等粘合。</td>
            </tr>
            <tr>
              <td>868A</td>
              <td> 淡黄色透明液体
              </td>
              <td> CR/MMA
                接枝聚合物
              </td>
              <td> 2200±
                200CP5
              </td>
              <td> 初期粘力好，耐热性好。对PVC、橡胶、TPR、
              EVA有很好的粘合力。</td>
            </tr>
            <tr>
              <td>870</td>
              <td> 无色半透明粘液
              </td>
              <td> CR/MMA
                接枝聚合物
              </td>
              <td> 5000±
                100CP5
              </td>
              <td> 耐黄性佳，适用于布/EVA、EVA/EVA之粘合。机
              械涂布单面上胶的专长，亦可用于篮球生产。</td>
            </tr>
          </table>
        </div>
      </div>
    );
  }
}
