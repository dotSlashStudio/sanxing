import React, {Component, Proptypes} from 'react';

export default class Series6 extends Component {

  render() {
    return (
      <div>
        <div className="profile-title">
          <span className="title-cn">处理剂系列</span>
        </div>
        <div>
          <table className="table table-bordered table-striped table-hover">
            <tr>
              <th colSpan="2"> 产品名称</th>
              <th>外观
             </th>
              <th>主成分<br />
             </th>
              <th> 主要用途及特性<br />
             </th>
            </tr>
            <tr>
              <td rowSpan="4"> 橡胶<br />
                处理剂<br />
                 801A</td>
              <td> 721<br />
             </td>
              <td>无色<br />
          透明液体<br />
             </td>
              <td>合成树脂<br />
             </td>
              <td> 处理效果显著，成分高，渗透性强。橡胶材料表<br />
                面专用处理剂，处理同类型之处理剂。<br />
                  </td>
            </tr>
            <tr>
              <td> 801F+B</td>
              <td> 无色<br />
                透明液体+B粉<br />
             </td>
              <td>合成树脂<br /></td>
              <td> 双组份橡胶处理效果强，性能显著、橡胶、牛筋<br />
              底、综合表面处理剂，白色橡胶材质处理。<br />     </td>
            </tr>
            <tr>
              <td>801GA+B<br />
               </td>
              <td>   无色<br />
                透明液体+B粉<br />
               </td>
              <td>合成树脂<br />
               </td>
              <td> 双组份橡胶处理效果强，性能显著、橡胶、牛筋<br />
                底、综合表面处理剂，白色橡胶材质处理。<br />
               </td>
            </tr>
            <tr>
              <td> 801A+B<br />
               </td>
              <td> 无色<br />
                透明液体+B粉<br />
               </td>
              <td>合成树脂<br />
               </td>
              <td> 双组份橡胶处理效果强，性能显著、橡胶、牛筋<br />
                底、综合表面处理剂。<br />
             <br /></td>
            </tr>
            <tr>
              <td rowSpan="3"> TPR<br />
                处理剂<br />
             </td>
              <td>894T<br />
             </td>
              <td> 无色<br />
                透明液体<br />
               </td>
              <td>合成树脂<br />
               </td>
              <td> 双组份橡胶处理效果强，成分高，渗透性强。TPR<br />
                、橡胶材料表面专用处理剂。处理同类型之处理特<br />
                别是对高密度、高杂质的TPR有很强的亲合力，处<br />
                理效果佳。<br />
             </td>
            </tr>
            <tr>
              <td>892T<br />
               </td>
              <td> 无色<br />
                透明液体<br />
               </td>
              <td>合成树脂<br />
               </td>
              <td> 双组份橡胶处理效果强，成分高，渗透性强。主<br />
                要针对TPR、橡胶材料表面专用处理剂。处理同<br />
                类型之处理特别是对高密度、高杂质的TPR有很<br />
                强的亲合力，处理小效果佳。<br />
               </td>
            </tr>
            <tr>
              <td>894K1<br />
             </td>
              <td> 无色<br />
                透明液体<br />
               </td>
              <td>合成树脂<br />
               </td>
              <td> 配合B粉使用。处理效果显著。<br />
             </td>
            </tr>
            <tr>
              <td rowSpan="3"> EVA<br />
                处理剂<br />
             </td>
              <td>895A<br />
               </td>
              <td>浅黄色粘液<br />
               </td>
              <td>合成树脂<br />
               </td>
              <td>处理效果佳，渗透性强。EVA材料粘合前表面处<br />
          理，可配合PU胶、接枝胶在涂胶钱作材料表面处<br />
          理。特别是对高密度、高杂质的EVA有很强的亲<br />
          合力，处理效果佳。<br />
               </td>
            </tr>
            <tr>
              <td> 895E<br />
               </td>
              <td> 无色<br />
                透明液体<br />
               </td>
              <td>合成树脂<br />
               </td>
              <td> 处理效果强，性能显著EVA材质表面专用处理剂<br />
                ，处理同类型之处理剂。<br />     </td>
            </tr>
            <tr>
              <td> UV88<br />
               </td>
              <td> 无色<br />
                透明液体<br />
               </td>
              <td>合成树脂<br /></td>
              <td> UV光照剂，主要是和EVA、MD底的光照处理，<br />
                效果极佳。<br />
               </td>
            </tr>
          </table>
        </div>
      </div>
    )
  }
}
