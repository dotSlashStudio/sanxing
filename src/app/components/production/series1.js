import React, {Component, Proptypes} from 'react';

export default class Series1 extends Component {

  render() {
    return (
      <div>
        <div className="profile-title">
          <span className="title-cn">免洗底系列</span>
        </div>
        <div>
          <table className="table table-bordered table-striped table-hover">
            <tr>
              <th> 底型</th>
              <th> 编号</th>
              <th> 外观</th>
              <th> 粘度</th>
              <th> 主要用途及特性</th>
            </tr>
            <tr>
              <td valign="middle" rowSpan="3"> PU底聚氨酯底</td>
              <td> 83A</td>
              <td> 无色透明粘液</td>
              <td>1300</td>
              <td valign="middle" rowSpan="3"> 不变黄、初期粘力强，专用于PU底免洗底</td>
            </tr>
            <tr>
              <td> 83B</td>
              <td> 无色透明粘液</td>
              <td>1800</td>
            </tr>
            <tr>
              <td> 83C</td>
              <td> 无色透明粘液</td>
              <td>2500</td>
            </tr>
            <tr>
              <td valign="middle" rowSpan="3">TPR底</td>
              <td>88</td>
              <td> 无色透明粘液</td>
              <td>1300</td>
              <td valign="middle" rowSpan="3"> 不变黄初期粘力強，专用于TPR底免洗底</td>
            </tr>
            <tr>
              <td>88A</td>
              <td> 无色透明粘液</td>
              <td>1800</td>
            </tr>
            <tr>
              <td>88C</td>
              <td> 无色透明粘液</td>
              <td>2500</td>
            </tr>
            <tr>
              <td valign="middle" rowSpan="3"> 橡胶底/片底</td>
              <td>85A</td>
              <td> 无色透明粘液</td>
              <td>1300</td>
              <td valign="middle" rowSpan="3"> 不变黄、初期粘力强，专用于橡胶底免洗底</td>
            </tr>
            <tr>
              <td>85B</td>
              <td> 无色透明粘液</td>
              <td>1800</td>
            </tr>
            <tr>
              <td>85C</td>
              <td> 无色透明粘液</td>
              <td>2500</td>
            </tr>
            <tr>
              <td valign="middle" rowSpan="3"> TPU底</td>
              <td>81A</td>
              <td> 无色透明粘液</td>
              <td>1300</td>
              <td valign="middle" rowSpan="3"> 不变黄、初期粘力强，专用于TPU底免洗底</td>
            </tr>
            <tr>
              <td>81B</td>
              <td> 无色透明粘液</td>
              <td>1800</td>
            </tr>
            <tr>
              <td>81C</td>
              <td> 无色透明粘液</td>
              <td>2500</td>
            </tr>
            <tr>
              <td valign="middle" rowSpan="3"> EVA打粗底</td>
              <td>86A</td>
              <td> 无色透明粘液</td>
              <td>1300</td>
              <td valign="middle" rowSpan="3"> 不变黄、初期粘力强，专用于EVA粗底免洗</td>
            </tr>
            <tr>
              <td>86B</td>
              <td> 无色透明粘液</td>
              <td>1800</td>
            </tr>
            <tr>
              <td>86C</td>
              <td> 无色透明粘液</td>
              <td>2500</td>
            </tr>
          </table>
          <table className="table table-bordered table-striped table-hover">
            <tr>
              <th width="70"> 产品名称
              </th>
              <th width="110"> 外观
              </th>
              <th width="100"> 主要成分
              </th>
              <th> 粘度
              </th>
              <th> 主要用途及特性
              </th>
            </tr>
            <tr>
              <td>62</td>
              <td> 无色半透明粘液
              </td>
              <td> 聚氨酯树脂
              </td>
              <td> 1300±100CPS
              </td>
              <td> 不变黄、初期粘力强。适合橡胶、TPR、EVA、
                PVC与PVC、PU革材料之间的粘合，适合流水
              操作。</td>
            </tr>
            <tr>
              <td>69A</td>
              <td> 无色透明粘液
              </td>
              <td> 聚氨酯树脂
              </td>
              <td> 1600±100CPS
              </td>
              <td> 不变黄、初期粘力强。适合橡胶、TPR、EVA、
                PVC与PVC、PU革材料之间的粘合，适合流水
              操作。</td>
            </tr>
            <tr>
              <td>69B</td>
              <td> 无色透明粘液
              </td>
              <td> 聚氨酯树脂
              </td>
              <td> 1800±100CPS
              </td>
              <td> 不变黄，耐热性好，初期粘力强。用于网布、
              真皮、多孔性材料，特别适用于白色材料。</td>
            </tr>
            <tr>
              <td>61</td>
              <td> 无色半透明粘液
              </td>
              <td> 聚氨酯树脂
              </td>
              <td> 1800±100CPS
              </td>
              <td> 初粘力强，维持时间长，适用于真皮及皮层鞋
              子。</td>
            </tr>
            <tr>
              <td>62</td>
              <td> 无色半透明粘液
              </td>
              <td> 聚氨酯树脂
              </td>
              <td> 1800±100CPS
              </td>
              <td> 不变黄，粘性维持时间适中，耐热性能佳。
              </td>
            </tr>
            <tr>
              <td>63</td>
              <td> 无色半透明粘液
              </td>
              <td> 聚氨酯树脂
              </td>
              <td> 1600±100CPS
              </td>
              <td> 不变黄，初粘力强，涂布性好且耐高温效果好
                ，适用于PVC、橡胶、TPR、EVA等材质，适
              合流水线操作。</td>
            </tr>
            <tr>
              <td>64</td>
              <td> 无色透明粘液
              </td>
              <td> 聚氨酯树脂
              </td>
              <td> 1500±100CPS
              </td>
              <td> 室温接着，味浅粘性维持时间长，耐黄变。适
                用于PVC、PU、ABS制品及经过处理之橡胶、
              TPR、EVA、尼龙等材质的接着。</td>
            </tr>
            <tr>
              <td>65</td>
              <td> 无色半透明粘液
              </td>
              <td> 聚氨酯树脂
              </td>
              <td> 1600±100CPS
              </td>
              <td> 适用于室温接着及PVC、PU制品和经过处理
              的橡胶、TPR、EVA、尼龙等材质之接着。</td>
            </tr>
            <tr>
              <td>66</td>
              <td> 无色半透明粘液
              </td>
              <td> 聚氨酯树脂
              </td>
              <td> 1500±100CPS
              </td>
              <td> 味道浅，初期粘力强，适用于PVC、PU、
                ABS制品及经过处理之橡胶，TPR、EVA、
              尼龙等材质的接着。</td>
            </tr>
            <tr>
              <td>67</td>
              <td> 无色半透明粘液
              </td>
              <td> 聚氨酯树脂
              </td>
              <td> 1600±100CPS
              </td>
              <td> 粘性维持时间长，初粘力好。通用于PVC、
              橡胶、TPR、EVA等。</td>
            </tr>
            <tr>
              <td>68</td>
              <td>乳白色粘液
              </td>
              <td> 聚氨酯树脂
              </td>
              <td> 1800±100CPS
              </td>
              <td> 耐黄变和耐寒性能强，粘性维持时间长，特
              别适用于手工方式作业，可作为冷补胶。</td>
            </tr>
          </table>
        </div>
      </div>
    )
  }
}
