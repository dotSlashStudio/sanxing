import React, {Component, PropTypes} from 'react';

import Slider from 'react-slick';

import './HomeStyle.css';

export default class Home extends Component {

  render() {
    const settings = {
      dots: true,
      infinite: true,
      autoplay: true,
      speed: 1000,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
    };
    return (
      <div>
        <Slider {...settings} className="slider">
          <div>
            <img src={require('./image/slick1.jpg')} />
          </div>
          <div>
            <img src={require('./image/slick2.jpg')} />
          </div>
          <div>
            <img src={require('./image/slick3.jpg')} />
          </div>
        </Slider>
        <div className="container">
          <div className="row">
            <div className="col-md-4 indexs">
              <div className="margin-bottom-lg index-title clearfix">
                <div className="pull-left">
                  <span className="title-cn">关于三興</span>
                  <span className="title-en">Profiles</span>
                </div>
                <div className="pull-right">
                  <a href="#/profile">More</a>
                </div>
              </div>
              <a href="#/profile">
                <img src={require('./image/profile.jpg')} alt=""/>
              </a>

            </div>
            <div className="col-md-4 indexs">
              <div className="margin-bottom-lg index-title clearfix">
                <div className="pull-left">
                  <span className="title-cn">免洗底系列</span>
                  <span className="title-en">Product Information</span>
                </div>
                <div className="pull-right">
                  <a href="#/production/series1">More</a>
                </div>
              </div>
              <a href="#/production/series1">
                <img src={require('./image/product.jpg')} alt=""/>
              </a>
            </div>
            <div className="col-md-4 indexs">
              <div className="margin-bottom-lg index-title clearfix">
                <div className="pull-left">
                  <span className="title-cn">联系我们</span>
                  <span className="title-en">Contact us</span>
                </div>
                <div className="pull-right">
                  <a href="#/contact">More</a>
                </div>
              </div>
              <a href="#/contact">
                <img src={require('./image/contact.jpg')} alt=""/>
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
