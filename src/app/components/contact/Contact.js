import React, {Component, PropTypes} from 'react';


export default class Contact extends Component {


  componentDidMount() {
    window.onload = () => {
      let map = new qq.maps.Map(document.getElementById("container"), {
        center: new qq.maps.LatLng(22.956060,114.693560),
        zoom: 15
      });
      let marker = new qq.maps.Label({
        position: new qq.maps.LatLng(22.956060,114.693560),
        map: map,
        content: '三兴化工（惠东）有限公司'
      })
    }()
  }

  render() {
    return (
      <div>
        <div className="banner margin-bottom-lg">
          <img src={require('./images/banner.png')} alt=""/>
        </div>
        <div className="container margin-top-xl">
          <div className="profile-title margin-bottom-lg">
            <span className="title-cn">联系我们</span>
            <span className="title-en">Contact Us</span>
          </div>
          <div className="row">
            <div className="col-md-9">
              <div id="container"></div>
            </div>
            <div className="col-md-3">
              <h5 className="margin-bottom-lg strong">三兴化工（惠东）有限公司</h5>
              <p className="margin-top-xl">电话： 0752-8924698</p>
              <p>传真： 0752-8915878</p>
              <p>QQ： 531895997</p>
              <p>微信： 13809696379</p>
              <p>地址： 广东省惠东县平山镇民营科技工业园</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
