import React, {Component, Proptypes} from 'react';

export default class Brand extends Component {

  render() {
    return (
      <div>
        <div className="profile-title">
          <span className="title-cn">品牌理念</span>
          <span className="title-en">Brand Concept</span>
        </div>
        <div>
          <div className="title2">
            <i className="fa fa-tags margin-right-md"></i>
            <span>善缘</span>
          </div>
          <p>
          大千世界,人是万物之灵;世界如此之大,人与人之间能相识,能合作,都是缘分,但是缘分有三种:善缘,孽缘,无缘。我们要珍 惜彼此的善缘,抛弃孽缘,建立有缘。

          </p>
          <div className="title2">
            <i className="fa fa-tags margin-right-md"></i>
            <span>平等</span>
          </div>
          <p>
          三興人认为:人与人之间,在尊严人格上是绝对平等的,而无高低贵贱之分,或许会有社会定位和分工不同,但在人格上是绝对平等。

          </p>
          <div className="title2">
            <i className="fa fa-tags margin-right-md"></i>
            <span>诚信</span>
          </div>
          <p>
          人无信则不立,亲人之间,朋友之间,合作伙伴之间都需要彼此诚信,只有诚信,大家才能共好。

          </p>
          <div className="title2">
            <i className="fa fa-tags margin-right-md"></i>
            <span>合作</span>
          </div>
          <p>
            在生活中,个人的精力与能力都是有限,不可能我们自己什么都可以干的了,需要彼此携手合作,互相帮助,只有这样,我们的生 活,我们的社会才会越变越好;而不是谁求谁。因此我们每个人,每家企业在做强自己的同时,更需要有与他人合作的心态。

          </p>
          <div className="title2">
            <i className="fa fa-tags margin-right-md"></i>
            <span>担当</span>
          </div>
          <p>
          三興人认为,人来到世界上,要生存发展,过上幸福美好生活,都需要付出劳动和承担责任,比如我们三興公司,如果终端用户使用 了我们三興的产品,若确实是我们质量有问题,而导致用户承受损失,我们三興会主动、勇敢、担当我们的责任,而不是推卸责任。

          </p>
          <div className="title2">
            <i className="fa fa-tags margin-right-md"></i>
            <span>公平</span>
          </div>
          <p>
          人与人之间的合作,只有合作的商业规则对双方均公平,合作双方才能走的长远,反之,彼此合作只能是暂时,且是无前景的。

          </p>
          <div className="title2">
            <i className="fa fa-tags margin-right-md"></i>
            <span>共赢</span>
          </div>
          <p>
          在我们三興公司,我认为,企业的不断发展,不仅仅是公司股东获利,我们同时会平衡好:供应商,企业员工,经销商,终端用户的 利益。在我们整个产业链中,我们希望所有的参与者都能获取合理的回报。能携手合作,共赢未来。

          </p>
          <div className="title2">
            <i className="fa fa-tags margin-right-md"></i>
            <span>快乐</span>
          </div>
          <p>
            我们三興人认为:人生下来不是只为了工作,而是来享受美好生活的。工作只是我们获取物质的方法。在生活中,有一种人只会奋斗 赚钱,没有生活,眼里只有钱,人生进入一个死循环,有了财富之后,对自己、对亲人、对员工都不好,成了财富的奴隶。而不是努 力奋斗之后,有了成果,去成为快乐,成为快乐之后又去奋斗,取得了成果之后,又去成为快乐,从而形成一个良性的循环,因为快 乐才是我们人生的终极目标,如果您不快乐,有再多的财富也无意义。

          </p>
        </div>
      </div>
    )
  }
}
