import React, {Component, Proptypes} from 'react';

export default class Human extends Component {

  render() {
    return (
      <div>
        <div className="profile-title">
          <span className="title-cn">人力理念</span>
          <span className="title-en">Concept Of Human</span>
        </div>
        <div>
          <div className="title2">
            <i className="fa fa-user margin-right-md"></i>
            <span>识人之明</span>
          </div>
          <p>
          三興本着“用人不疑、疑人不用”的原则,人尽其才,让忠诚于三興事业的人到合适的岗位上去,给员工提供充分展现个人才华 与实现个人价值的舞台

          </p>
          <div className="title2">
            <i className="fa fa-user margin-right-md"></i>
            <span>宽松的发展空间与平台</span>
          </div>
          <p>
          本着开发式人力资源管理理念,三興给员工充分的自主权,员工无须扬鞭自奋蹄。多年来,三興海纳人才,大部分中高层领导从 内部提拔,人员稳定坚固,大多数中高层都伴随三興成长超过五年以上,汇集成了一支睿智、上进、敬业、忠诚、勤奋、廉洁自 律的三興精英团队。

          </p>
          <div className="title2">
            <i className="fa fa-user margin-right-md"></i>

            <span>鞭策员工随企业一起成长</span>
          </div>
          <p>
          逆水行舟,不进则退”为促使员工随企业一起成长,三興在员工培训方面投入了大量人力、物力、财力,成立了培训学院, 培训采用常年制度化管理,通过内请、外聘和送出培训等方式,对公司所有员工进行产品、技术、操作、质量体系、营销、管理 等方面的系统培训。 

          </p>
          <p>为帮组员工实现个人价值,三興和多家国内知名培训学习机构建立长期合作关系,定期为企业员工提供系统培训,保证员工随三 興事业同步前进。</p>
          <p>在三興,每个人努力付出时,人生价值都会在工作中得到彰显; </p>
          <p>在三興,每个人享受辛勤工作的丰厚回报时,人生目标都会在愉悦的工作环境中更加清晰!</p>
          <div className="title2">
            <i className="fa fa-user margin-right-md"></i>
            <span>选拔人才原则</span>
          </div>
          <p>
          以内部培养为主,外部招聘为辅,坚持三公(公开、公正、公平)原则,注重培养具有大局观念勇于创新的专才。

          </p>
          
        </div>
      </div>
    )
  }
}