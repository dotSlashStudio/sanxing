import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';

import './CultureStyle.css';

export default class Culture extends Component {

  render() {
    return (
      <div>
        <div className="banner margin-bottom-lg">
          <img src={require('./images/banner.jpg')} alt=""/>
        </div>
        <div className="container">
          <div className="margin-top-lg row">
            <div className="sidebar col-xs-3">
              <Link to="/culture/enterprise" activeClassName="active">
                <i className="fa fa-th-large margin-right-md"></i>
                <span>企业文化</span>
              </Link>
              <Link to="/culture/brand" activeClassName="active">
                <i className="fa fa-th-large margin-right-md"></i>
                <span>品牌理念</span>
              </Link>
              <Link to="/culture/human" activeClassName="active">
                <i className="fa fa-th-large margin-right-md"></i>
                <span>人力理念</span>
              </Link>
            </div>
            <div className="main-content col-xs-9">
              {this.props.children}
            </div>
          </div>  
        </div>
      </div>
    );
  }
}
