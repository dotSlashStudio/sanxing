import React, {Component, Proptypes} from 'react';

export default class Enterprise extends Component {

  render() {
    return (
      <div>
        <div className="profile-title">
          <span className="title-cn">企业文化</span>
          <span className="title-en">Enterprise Culture</span>
        </div>
        <div>
          <div className="title2">
            <i className="fa fa-send-o margin-right-md"></i>
            <span>使命与愿景</span>
          </div>
          <p>
          “中国胶粘剂因三興而骄傲”从建厂伊始,三興人始终没有忘记自己的使命和愿景:“中国 胶粘剂因三興而骄傲。”正是怀着这个远大的梦想,三興才从荆棘丛生中—步步走来,并日 渐成为了中国胶粘剂的一面旗帜。为达成这一使命,三興致力于为客户营造“环保健康的产 品”,专注于健康、环保产品的研发和销售,誓做“健康环保胶粘剂第一品牌”,并通过优 质的服务让每一位客户体会到实在的利益:健康环保的品质,细致便捷的服务和一流的性价 比。在“中国粘合剂因三興而骄傲“这一使命的指导下,三興将建设成为专业化、服务型和 拥有独立产业链的粘合剂及相关服务业的综合企业集团。

          </p>
          <div className="title2">
            <i className="fa fa-send-o margin-right-md"></i>
            <span>核心价值观</span>
          </div>
          <p>
          在长期的实践中,三興的员工一直坚守着这样的信念和判断是非的标准,以此形成了独具特 色又被高度认同的企业文化。

          </p>
          <div className="title2">
            <i className="fa fa-send-o margin-right-md"></i>
            <span>小赢于智,大赢于德</span>
          </div>
          <p>
          靠智慧可以获得初级成功,只有靠良好的品德才能使企业做大做强。 三興人讲究以诚为本,以信取人,在诚信中生存,在创新中发展。说了的事—定要办,所办 的事一定办好。以卓越的产品质量取信于民,以共同发展的理念诚德待人。对于害户、伙伴 、员工,分享利益,按照贡献获得合理回报。

          </p>
          <div className="title2">
            <i className="fa fa-send-o margin-right-md"></i>
            <span>偶胜于时,长胜于专</span>
          </div>
          <p>
          偶然的机遇可以让你获得短期的胜利,只有坚持不懈走专业化道路才能获得长久的胜利。 除了企业自身专注于胶粘剂领域的研发、生产与销售外,三興还要求员工和战略合作伙伴在 专业领域独当一面,成为所在领域真正的专家。因为三興知道,只有专业才能取得长久的成 功,才能保证企业的基业长青。

          </p>
          <div className="title2">
            <i className="fa fa-send-o margin-right-md"></i>
            <span>取利于民,还利于善</span>
          </div>
          <p>
          包含三层含义:以为社会提供优质环保的产品为己任;为员工提供就业的机会和良好的工作 环境,为地方纳税,促进社会稳定和地方发展,热心社会公益事业。 责任是令之企业文化中的重要内容。三興要求员工按世界一流企业的标准要求自己,提供高 品质的产品和服务。建立守法经营,员工信赖,消费者尊敬和认可的企业。追求长久发展, 社会效益和经济效益并重发展,竭尽所能帮助社会上最需要帮助的人。

          </p>

        </div>
      </div>
    )
  }
}