import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';

import DropNav from './DropNav';

export default class NavTabs extends Component {

  render() {
    return (
      <div className="header">
        <div className="container clearfix">
          <DropNav />
          <div className="logo pull-left">
            <img src={require('./images/logo.png')} />
          </div>
          <div className="pull-right">
            <div className="title">
              <img src={require('./images/title.png')} />
            </div>
            <div className="links">
              <Link to="/home" activeClassName="active">
                <span>首页</span>
              </Link>
              <Link to="/profile" activeClassName="active">
                <span>关于三興</span>
              </Link>
              <Link to="/culture/enterprise" activeClassName="active">
                <span>文化与理念</span>
              </Link>
              <Link to="/production/series1" activeClassName="active">
                <span>产品信息</span>
              </Link>
              <Link to="/technology/tech1" activeClassName="active">
                <span>技术档案</span>
              </Link>
              <Link to="/contact" activeClassName="active">
                <span>联系我们</span>
              </Link>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
