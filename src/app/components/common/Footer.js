import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';

export default class TopBar extends Component {

  render() {
    return (
      <div className="container footer">
        <div className="row">
          <div className="col-lg-9 col-md-5">
            <p>惠东县三兴化工有限公司 | All Rights Reserved | 粤ICP备14070745号-1</p>
            <div className="margin-bottom-md">
              <span>电话： 0752-8924698</span>
              <span className="margin-left-lg">传真： 0752-8915878</span>
            </div>
            <div className="margin-bottom-md">
              <span>QQ: 531895997</span>
              <span className="margin-left-lg">微信：13809696379</span>
            </div>
            <p>地址：广东省惠东县平山镇民营科技工业园</p>
          </div>
          <div className="col-lg-3 col-md-5">
            <img src={require('./images/qr.png')} className="pull-right" />
          </div>
        </div>
      </div>
    )
  }
}
