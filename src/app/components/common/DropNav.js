import React, {Component} from 'react';
import classnames from 'classnames';
import {Link} from 'react-router';

export default class DropNav extends Component {

  state = {
    hide: true,
  }
  handleClick(e) {
    e.preventDefault();
    this.setState({
      hide: !this.state.hide,
    })
  }
  render() {
    return (
      <div className="links-small-screen" onClick={::this.handleClick}>
        <i className="fa fa-bars"></i>
        <div className={classnames('link-items', {hide: this.state.hide})}>
          <Link to="/home" activeClassName="active">
            <span>首页</span>
          </Link>
          <Link to="/profile" activeClassName="active">
            <span>关于三興</span>
          </Link>
          <Link to="/culture/enterprise" activeClassName="active">
            <span>文化与理念</span>
          </Link>
          <Link to="/production/series1" activeClassName="active">
            <span>产品信息</span>
          </Link>
          <Link to="/technology/tech1" activeClassName="active">
            <span>技术档案</span>
          </Link>
          <Link to="/contact" activeClassName="active">
            <span>联系我们</span>
          </Link>
        </div>
      </div>
    )
  }
}
