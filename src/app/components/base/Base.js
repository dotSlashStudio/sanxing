import React, {Component} from 'react';

import Header from 'components/common/Header';
import Footer from 'components/common/Footer';

export default class Base extends Component {

 
  render() {
    return (
      <div>
        <div>
          <Header />
          <div>
            {this.props.children}
          </div>
          <Footer />
        </div>
      </div>
    );
  }
}
