import $ from 'jquery';
import config from 'config';

import './login.css';

$(() => {
  // $('#favicon').attr('href', require('./favicon.ico'));
  // $('#pic1').attr('src', require('./images/login-pic1.png'));
  $('form').on('submit', (e) => {
    e.preventDefault();
    $.ajax({
      method: 'POST',
      url: config.eadmin.baseUrl + '/oauth/signin',
      data: {
        email: $('#email').val(),
        password: $('#password').val(),
        client_id: config.clientId,
      },
      success: (data) => {
        localStorage.access_token = data.access_token;
        Cookies.set('accessToken', data.access_token);
        const match = window.location.href.match(/\?path=(.*)/);
        if (match) {
          window.location.replace('/#' + match[1]);
        } else {
          window.location.replace('/');
        }
      },
      error: () => {
        $('.error').removeClass('hide');
        setTimeout(() => {
          $('.error').addClass('hide');
        }, 2000);
      },
    });
  });
});
