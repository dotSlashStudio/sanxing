
module.exports = {

  enableLogger: false,
  enableDevtools: false,

  title: 'GCS',

  clientId: 'infosys',

  edata: {
    baseUrl: 'http://edata-api.ersinfotech.com',
  },

  eadmin: {
    baseUrl: 'http://eadmin-api.ersinfotech.com',
  },

  infosys: {
    baseUrl: 'http://infosys-api.ersinfotech.com',
  },

  php: {
    baseUrl: 'http://202.175.86.136/ers_api/index.php',
  },

  api: {
    baseUrl: 'http://api.e-research-solutions.com',
  },

  facebook: {
    baseUrl: 'https://graph.facebook.com',
  },
};
